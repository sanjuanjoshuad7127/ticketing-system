<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TicketController;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

*/
Route::get("/", [UserController::class, 'index']);
Route::get("/register", [UserController::class, 'get_register']);
Route::get("/login", [UserController::class, 'get_login']);
Route::post("/post_login", [UserController::class, 'login']);
Route::post("/post_register", [UserController::class, 'register']);
Route::get("/email/{email}", [UserController::class, 'email']);
Route::get("/code/{email}", [UserController::class, 'code']);
Route::post("/send_code/{email}", [UserController::class, 'send_code']);
Route::get("/receive_email", [UserController::class, 'receive_email']);
Route::get("/ticket", [TicketController::class, 'index']);
Route::post("/ticket/post_ticket", [TicketController::class, 'post_ticket']);
Route::get("/tickets", [TicketController::class, 'tickets']);
Route::get("/live_chat/{email}/{message_id}", [TicketController::class, 'get_live_chat']);
Route::get("/support_ticket", [TicketController::class, 'support_ticket']);
Route::get("/get_ticket_email", [TicketController::class, 'get_ticket_email']);
Route::post("/send_message/{email}/{message_id}", [TicketController::class, 'send_message']);
Route::get("/customer_tickets", [TicketController::class, 'customer_tickets']);
Route::get("/support_ticket/register", [TicketController::class, 'get_support_register']);
Route::post("/support_ticket/post_register", [TicketController::class, 'post_support_register']);
Route::get("/support_ticket/email/{email}", [TicketController::class, 'email']);
Route::get("/support_ticket/code/{email}", [TicketController::class, 'code']);
Route::post("/support_ticket/send_code/{email}", [TicketController::class, 'send_code']);
Route::get("/support_ticket/login", [TicketController::class, 'get_login']);
Route::post("/support_ticket/post_login", [TicketController::class, 'login']);
Route::put("/update_live_chat/{message_id}", [TicketController::class, 'update_live_chat']);
Route::get("/customers", [TicketController::class, 'customers']);
Route::get("/customers_tickets/{email}", [TicketController::class, 'customers_tickets']);

/*
Route::get('/oauth/gmail', function (){
    return LaravelGmail::redirect();
});

Route::get('/oauth/gmail/callback', function (){
    LaravelGmail::makeToken();
    return redirect()->to('/support_ticket');
});


Route::get('/oauth/gmail/logout', function (){
    LaravelGmail::logout(); //It returns exception if fails
    return redirect()->to('/support_ticket');
});
*/

Route::get("/oauth/gmail", [TicketController::class, 'redirect_to_google']);

Route::get('/oauth/gmail/callback', function (Request $request){
   // LaravelGmail::makeToken();
   $user = Socialite::driver('google')->user();
   $request->session()->put('customer', $user);
    return redirect()->intended('/support_ticket');
});


Route::get('/oauth/gmail/logout', function (Request $request){
    $request->session()->forget('customer');
    return redirect()->to('/support_ticket');
});
