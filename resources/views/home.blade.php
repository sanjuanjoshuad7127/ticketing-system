
@extends('layout/nav')
@section ('content')
<br>
<div class="d-flex">
<div class="hello">
Hello
</div>  

<div class="username">{{Session::get('user')->name}}</div> 

</div>
<br>

<div class="mx-5">

<table class="table table-hover border-dark">
<caption>List of all tickets</caption>

<thead>
    <tr>
      <th scope="col">Requester</th>
      <th scope="col">Reported By</th>
      <th scope="col">Description</th>
      <th scope="col">Type</th>
      <th scope="col">Status</th>
      <th scope="col">Severity</th>
      <th scope="col">Priority</th>
      <th scope="col">Module</th>
      <th scope="col">Remarks</th>
      <th scope="col">Date Reported</th>
      <th scope="col">Date Accomplishment</th>
    
      
    </tr>
</thead>

<tbody>

@foreach($tickets as $ticket)
<tr>
<td scope="row"><a href="{{ url('live_chat/'.$ticket->customer_email.'/'.$ticket->message_id) }}">{{$ticket->requester}}</a></td>
<td>{{($ticket->reported_by == null)?'None':$ticket->reported_by}}</td>
<td>{{$ticket->description}}</td>
<td>{{($ticket->type == null)?'None':$ticket->type}}</td>
<td>{{($ticket->status == null)?'No Status':$ticket->status}}</td>
<td>{{($ticket->severity == null)?'No Severity':$ticket->severity}}</td>
<td>{{($ticket->priority == null)?'None':$ticket->priority}}</td>
<td>{{($ticket->module == null)?'None':$ticket->module}}</td>
<td>{{($ticket->remarks == null)?'None':$ticket->remarks}}</td>
<td>{{($ticket->date_reported == null)?'None':$ticket->date_reported}}</td>
<td>{{($ticket->date_accomplishment == null)?'None':$ticket->date_accomplishment}}</td>

</tr>

@endforeach

</tbody>
</table>
</div>






@stop
