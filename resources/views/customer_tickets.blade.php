<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.header')
</head>
<body>

<div class="mx-5">

<table class="table table-hover border-dark">
<caption>List of tickets</caption>

<thead>
    <tr>
      
      <th scope="col">Description</th>
      <th scope="col">Reported By</th>
      <th scope="col">Severity</th>
      <th scope="col">Status</th>
    </tr>
</thead>

<tbody>

@foreach($tickets as $ticket)
<tr>
<td scope="row"><a href="{{ url('live_chat/'.env('IMAP_USERNAME').'/'.$ticket->message_id) }}">{{$ticket->description}}</a></td>
<td>{{($ticket->reported_by == null)?'None':$ticket->reported_by}}</td>
<td>{{($ticket->severity == null)?'No Severity':$ticket->severity}}</td>
<td>{{($ticket->status == null)?'No Status':$ticket->status}}</td>
</tr>

@endforeach

</tbody>
</table>
</div>


<script type="text/javascript" src="{{ asset('js/myapp.js') }}"></script>
</body>
</html>