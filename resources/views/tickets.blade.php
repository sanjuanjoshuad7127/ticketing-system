@extends('layout/nav')
@section ('content')
<br>

<div class="mx-5">

<table class="table table-hover border-dark">
<caption>List of tickets</caption>

<thead>
    <tr>
      <th scope="col">Requester</th>
      <th scope="col">Reported By</th>
      <th scope="col">Description</th>
      <th scope="col">Severity</th>
      <th scope="col">Status</th>
    </tr>
</thead>

<tbody>

@foreach($tickets as $ticket)
<tr>
<td scope="row"><a href="{{ url('live_chat/'.$ticket->customer_email.'/'.$ticket->message_id) }}">{{$ticket->requester}}</a></td>
<td>{{($ticket->reported_by == null)?'None':$ticket->reported_by}}</td>
<td>{{$ticket->description}}</td>
<td>{{($ticket->severity == null)?'No Severity':$ticket->severity}}</td>
<td>{{($ticket->status == null)?'No Status':$ticket->status}}</td>
</tr>

@endforeach

</tbody>
</table>
</div>


@stop
