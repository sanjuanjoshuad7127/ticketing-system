<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.header')
</head>
<body>

<br>
<div class="d-flex justify-content-center">
    <form class="px-2 bg-dark text-white rounded" style="width: 30%;" action="/post_register" method="post">

    {{ csrf_field() }}

    <div class="m-3">
        <label for="name" class="form-label">Name</label>
    <input class="form-control" type="text" name="name" id="name" placeholder="Name">

    @if($errors->has('name') )
    <div class="form-text text-danger">{{ $errors->first('name') }}</div>

@endif

</div>

<div class="m-3">
        <label for="email" class="form-label">Email</label>

    <input class="form-control" type="text" name ="email" id="email" placeholder="Email">

    @if($errors->has('email') )
    <div class="form-text text-danger">{{ $errors->first('email') }}</div>
@elseif($errors->has('emailError'))
<div class="form-text text-danger">{{ $errors->first('emailError') }}</div>
@endif
</div>


<div class="m-3">
        <label for="pass" class="form-label">Password</label>
<input class="form-control" type="password" name="pass" id="pass" placeholder="Password">
@if($errors->has('pass') )
<div class="form-text text-danger">{{ $errors->first('pass') }}</div>
@endif
</div>

<div class="m-3">
        <label for="cpass" class="form-label">Confirm Password</label>
<input class="form-control" type="password" name="cpass" id="cpass" placeholder="Confirm Password">
@if($errors->has('cpass') )
<div class="form-text text-danger">{{ $errors->first('cpass') }}</div>
@elseif($errors->has('passError') )
<div class="form-text text-danger">{{ $errors->first('passError') }}</div>
@endif
<br>
</div>

<div class="m-3">
    <button class="form-control btn btn-primary btn-lg" type="submit">Register</button>
</div>
<br>
    </form>

</div>
    

    <script type="text/javascript" src="{{ asset('js/myapp.js') }}"></script>
</body>
</html>