<!DOCTYPE html>
<html lang="en">
<head>
@include('layout.header')
</head>
<body>



<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<div class="collapse navbar-collapse">
<div class="navbar-nav">
<a class ="nav-item nav-link" href="/">Home</a>
<a class = "nav-item nav-link" href="/tickets">Tickets</a>
<a class = "nav-item nav-link" href="/customers">Customers</a>
</div>
</div>
</nav>

@yield ('content')



<script type="text/javascript" src="{{ asset('js/myapp.js') }}"></script>

</body>
</html>