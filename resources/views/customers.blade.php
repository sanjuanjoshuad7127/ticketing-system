@extends('layout/nav')
@section ('content')




<div class="mx-5">

<table class="table table-hover border-dark">

<thead>
    <tr>
      
      <th scope="col">Requester</th>
      <th scope="col">Number of Tickets</th>
      
    </tr>
</thead>

<tbody>

@foreach($customers as $customer)
<tr>
<td scope="row"><a href="{{ url('customers_tickets/'.$customer->customer_email) }}">{{$customer->requester}}</a></td>
<td>{{$customer->total}}</td>

</tr>

@endforeach

</tbody>
</table>
</div>




@stop