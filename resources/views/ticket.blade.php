<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.header')
</head>
<body>

<div class="m-4">
<form action="/ticket/post_ticket" method="post">

{{ csrf_field() }}


<label for="description" class="form-label">Description</label>

<input class="form-control border-dark" style="width:50%;" type="text" name="description" id ="description" placeholder="Description">
@if($errors->has('description') )
<div class="form-text text-danger">{{ $errors->first('description') }}</div>

@endif
<br>
<textarea class="form-control border-dark" style="width:50%;" name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
@if($errors->has('message') )

<div class="form-text text-danger">{{ $errors->first('message') }}</div>
@endif
<br>
<button class="btn btn-outline-dark" style="width: 30%;" type="submit">Send</button>

</form>

</div>

<script type="text/javascript" src="{{ asset('js/myapp.js') }}"></script>
</body>
</html>