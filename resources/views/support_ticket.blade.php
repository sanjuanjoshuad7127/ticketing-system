<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.header')
</head>
<body>

<br>
<div class="mx-4">
@if(session()->get('customer') != NULL)
{{session()->get('customer')->name}}
<br>
{{session()->get('customer')->email}}
<br>
<a class="btn btn-outline-success mt-3" role="button" href="{{ url('/ticket') }}">Make a ticket</a>
<br>
<a class="btn btn-outline-primary mt-2" role="button" href="{{url('/customer_tickets')}}">My Tickets</a>
<br>
<a class="btn btn-outline-danger mt-2" role="button" href="{{ url('oauth/gmail/logout') }}">logout</a>

@else
<a class="btn btn-outline-info mt-2" role="button" href="{{ url('oauth/gmail') }}">login to gmail</a>
<br>
<a class="btn btn-outline-primary mt-2" role="button" href="{{ url('support_ticket/login') }}">login</a>
<br>
<a class="btn btn-outline-secondary mt-2" role="button" href="{{ url('support_ticket/register') }}">register</a>
@endif
</div>

<script type="text/javascript" src="{{ asset('js/myapp.js') }}"></script>
</body>
</html>