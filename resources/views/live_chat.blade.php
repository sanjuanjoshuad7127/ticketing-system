<!DOCTYPE html>
<html lang="en">
<head>
@include('layout.header')



</head>
<body>
    



<div class="container">
<div class="row">

<div class="col col-8">
<!-- filter per support -->
<br>

<div class="h-10 fs-3 px-3 border border-dark border-1 text-wrap rounded" style="font-weight: 550;">{{$ticket->description}}</div>
<br>
<div class="scroll px-3 border border-dark border-1 rounded" id="scroll">
<div class="live_chat_content">

@foreach($messages as $message)
<div class="live_chat{{$loop->iteration}}">
@php($text = '')
@if($message->receiver == $email)
   @php($text = 'text-primary')

@else
    @php($text = 'text-secondary')

@endif

@if($message->receiver == env('IMAP_USERNAME'))
<div class="fs-5 mb-2 {{$text}}">{{$ticket->requester}} </div>
@endif
@if($message->sender == env('IMAP_USERNAME'))
<div class="fs-5 mb-2 {{$text}}">{{env('MAIL_FROM_NAME')}} </div>
@endif
{!! ($message->message) !!}
<br>
@if($loop->count != $loop->iteration)
<hr/>
@endif
<br>
</div>


@endforeach

</div>
</div>


<br>
<form action="{{ url('send_message/'.$email.'/'.$message_id) }}" method="post">
{{ csrf_field() }}
<textarea class = "h-10 p-3 border border-dark border-1 rounded" placeholder="Reply" name="message" id="message" style="resize: none; width: 100%;" cols="30" rows="7"></textarea>
<br>
<button type="button" class="btn btn-outline-dark send_button" id="send">Send</button>

</form>
</div>

<div class="col scroll">

<br>
<form action="/update_live_chat/{{$message_id}}" method="post">
@csrf
@method('put')

<div class="mb-3">
<label for="" class="form-label">Reported by</label>
<input class="form-control border-dark" type="text" name="reported_by" value="{{$ticket->reported_by}}" placeholder="Reported by">
@if($errors->has('module') )
<div class="form-text text-danger">{{ $errors->first('module') }}</div>
@endif
</div>

<div class="mb-3">
<label for="" class="form-label">Description</label>
<input class="form-control border-dark" type="text" name="description" value="{{$ticket->description}}" readonly>
</div>

<div class="mb-3">
<label for="type" class="form-label">Type</label>
<select class="form-select border-dark" name="type" id="type">
    <option value="Defect/Bugs" {{($ticket->type == 'Defect/Bugs')?'selected':''}}>Defect/Bugs</option>
    <option value="New Request" {{($ticket->type == 'New Request')?'selected':''}}>New Request</option>
    <option value="Change Request" {{($ticket->type == 'Change Request')?'selected':''}}>Change Request</option>
</select>

</div>

<div class="mb-3">
<label for="status" class="form-label">Status</label>
<select class="form-select border-dark" name="status" id="status">
    <option value="Pending">Pending</option>
    <option value="In Progress Development" {{($ticket->status == 'In Progress Development')?'selected':''}}>In Progress Development</option>
    <option value="For Deployment and Testing" {{($ticket->status == 'For Deployment and Testing')?'selected':''}}>For Deployment and Testing</option>
    <option value="Operator to Confirm & Check" {{($ticket->status == 'Operator to Confirm & Check')?'selected':''}}>Operator to Confirm & Check</option>
    <option value="Done" {{($ticket->status == 'Done')?'selected':''}}>Done</option>
</select>

</div>

<div class="mb-3">
<label for="severity" class="form-label">Severity</label>
<select class="form-select border-dark" name="severity" id="severity">
    <option value="Major" {{($ticket->severity == 'Major')?'selected':''}}>Major</option>
    <option value="Minor" {{($ticket->severity == 'Minor')?'selected':''}}>Minor</option>
</select>

</div>

<div class="mb-3">
<label class ="form-label" for="priority">Priority</label>
<input type="number" class="form-control border-dark" name="priority" id="priority" value="{{($ticket->priority=='')?'0':$ticket->priority}}" min="0" max ="10">
@if($errors->has('priority') )
<div class="form-text text-danger">{{ $errors->first('priority') }}</div>
@endif
</div>


<div class="mb-3">
<label class ="form-label" for="module">Module</label>
<input type="text" class="form-control border-dark" name="module" id="module" value="{{$ticket->module}}" placeholder="Module">
@if($errors->has('module') )
<div class="form-text text-danger">{{ $errors->first('module') }}</div>
@endif
</div>

<div class="mb-3">
<label class ="form-label" for="date_reported">Date reported</label>
<input type="date" class="form-control border-dark" name="date_reported" id="date_reported" value="{{$ticket->date_reported}}">
@if($errors->has('date_reported') )
<div class="form-text text-danger">{{ $errors->first('date_reported') }}</div>
@endif
</div>

<div class="mb-3">
<label class ="form-label" for="date_accomplishment">Date of accomplishment</label>
<input type="date" class="form-control border-dark" name="date_accomplishment" id="date_accomplishment" value="{{$ticket->date_accomplishment}}">
@if($errors->has('date_accomplishment') )
<div class="form-text text-danger">{{ $errors->first('date_accomplishment') }}</div>
@endif
</div>

<div class="mb-3">
<label class ="form-label" for="remarks">Remarks</label>
<textarea class="form-control border-dark" name="remarks" id="remarks" rows="5" placeholder="Remarks">{{$ticket->remarks}}</textarea>
@if($errors->has('remarks') )
<div class="form-text text-danger">{{ $errors->first('remarks') }}</div>
@endif
</div>

<div class="mb-3">
<button type="submit" class="btn btn-outline-dark" style="width: 50%;">Submit</button>

</div>

</form>


</div>



</div>
</div>



<script>

var conn = new WebSocket('ws://127.0.0.1:8080');

var conn3 = new WebSocket('ws://127.0.0.1:8070');

const live_chat_object = {
    sent: "not sent",
    message_id: "{{$message_id}}"

};

const scroll = document.getElementById("scroll");

scroll.scrollTop = scroll.scrollHeight;

conn.onopen = function(){

console.log('connection success');


//live_chat_object.sent = "not sent";

conn.send(JSON.stringify(live_chat_object));


live_chat_object.sent = "";


};

let ex = 'not success';
let text = "";
let live_chat_content = document.getElementsByClassName('live_chat_content')[0];
let sender_receiver = "";
let color_text = "";

conn.onmessage = function(e){
    try{
        let data = JSON.parse(e.data);
   if(typeof data === 'object' && data.name == 'live_chat'){
    console.log(data.message);



    



    let live_chat_class = (Number(live_chat_content.lastElementChild.className.match(/\d+/)[0])+1).toString();

console.log('click');
console.log((Number(live_chat_content.lastElementChild.className.match(/\d+/)[0])+1).toString());
document.getElementsByClassName(live_chat_content.lastElementChild.className)[0].lastElementChild.remove();

const newNode = document.createElement("div");

if(data.receiver == "{{$email}}"){
    color_text = "text-primary";
}
else{
    color_text = "text-secondary";
}

if(data.receiver == "{{env('IMAP_USERNAME')}}"){
    sender_receiver = "{{$ticket->requester}}";
}

if(data.sender == "{{env('IMAP_USERNAME')}}"){
    sender_receiver = "{{env('MAIL_FROM_NAME')}}";
}


newNode.className = 'live_chat' + live_chat_class;
//text = document.createTextNode("joshua");
//text = '<div dir="ltr">ggg</div><br><div class="gmail_quote"><div dir="ltr" class="gmail_attr">Noong Huw, Hun 22, 2023 nang 11:54 AM, sinulat ni Support &lt;<a href="mailto:aaronsanjuan1717@gmail.com">aaronsanjuan1717@gmail.com</a>&gt; ang:<br></div><blockquote class="gmail_quote" style="margin:0px 0px 0px 0.8ex;border-left:1px solid rgb(204,204,204);padding-left:1ex"><u></u> <div> <h3>Re: this is my test 2</h3> <p>qaqa</p> </div> </blockquote></div>';
text = data.message;
newNode.innerHTML = '<hr> <br>' + '<div class="fs-5 mb-2 ' + color_text + '">' + sender_receiver + '</div>' + text + '<br> <br>';

live_chat_content.insertBefore(newNode, null);

//console.log(document.getElementsByClassName('gmail_quote'))

//--


let gmail_quote = document.getElementsByClassName('gmail_quote');
let check = "false";
let myindex = 0;

let mybutton = document.createElement('button');
var dots = new Image(30,10);
dots.src = "{{asset('images/threedots.png')}}";

mybutton.setAttribute('id', 'toggle_reply'+live_chat_class);
mybutton.setAttribute('type', 'button');
mybutton.setAttribute('class', 'dots');

mybutton.appendChild(dots);
//mybutton.textContent = '...';

for(let i=0;i<gmail_quote.length;i++){
if(gmail_quote[i].parentNode.className == "live_chat"+live_chat_class){

    gmail_quote[i].style.display = 'none';
    check = "true";
    myindex = i;
}

}

gmail_quote[myindex].parentNode.insertBefore(mybutton, gmail_quote[myindex]);

if(check == "false"){
    document.getElementById("toggle_reply"+live_chat_class).style.display = 'none';
}

document.getElementById("toggle_reply"+live_chat_class).addEventListener("click", function(){

for(let i=0;i<gmail_quote.length;i++){
if(gmail_quote[i].parentNode.className == "live_chat"+live_chat_class){

    console.log(gmail_quote[i].parentNode.className);

    if(gmail_quote[i].style.display == 'none'){
        gmail_quote[i].style.display = '';
    }
    else{
        gmail_quote[i].style.display = 'none';
    }

   
}

    
}
});





scroll.scrollTop = scroll.scrollHeight;


   }
    }
    catch(error){
        console.log(e.data);
    }
    

 ex = e.data;
 
 //timeout1(ex);


};

/*
function timeout1(ex) {
    setTimeout(function () {
        if(ex == 'success'){
        ex = 'not success';
    conn.send(JSON.stringify(live_chat_object));
  //  conn.send('');
//console.log('Success');
timeout1(ex);
    }
        
    }, 10);
}
*/

 



//--conn2


/*
 function timeout2(ex2, ex) {
    setTimeout(function () {
        if(ex2 == 'success'){
        ex2 = 'not success';
  //  conn.send(JSON.stringify(live_chat_object));
    conn2.send('');
console.log('Success');
timeout2(ex2);
    }
    else{
        
        conn.send(JSON.stringify(live_chat_object));
        console.log('success');
        timeout2(ex2);
    }
        
    }, 100);
}
*/


setInterval(function(){

    if(ex == 'success'){
        ex = 'not success';
    conn.send(JSON.stringify(live_chat_object));
    }

 // console.log('test');
    
}, 10);










let send = document.getElementById('send');
let message = document.getElementById('message');


send.addEventListener("click", function(){

if(message.value != ''){

const live_chat_object_send = {
    sent: "sent",
    message_id: "{{$message_id}}",
    message: message.value,
    sender: "{{($email == env('IMAP_USERNAME'))?(session()->get('customer')->email):(env('IMAP_USERNAME'))}}",
    receiver: "{{$email}}"


};

conn.send(JSON.stringify(live_chat_object_send));

@if($email != env('IMAP_USERNAME'))
conn3.send(JSON.stringify(live_chat_object_send));
@endif
message.value = "";

//live_chat_object_send.sent = "email sent";
//conn.send(JSON.stringify(live_chat_object_send));



/*
let live_chat_class = (Number(live_chat_content.lastElementChild.className.match(/\d+/)[0])+1).toString();

console.log('click');
console.log((Number(live_chat_content.lastElementChild.className.match(/\d+/)[0])+1).toString());

const newNode = document.createElement("div");
newNode.className = 'live_chat' + live_chat_class;
//text = document.createTextNode("joshua");
//text = '<div dir="ltr">ggg</div><br><div class="gmail_quote"><div dir="ltr" class="gmail_attr">Noong Huw, Hun 22, 2023 nang 11:54 AM, sinulat ni Support &lt;<a href="mailto:aaronsanjuan1717@gmail.com">aaronsanjuan1717@gmail.com</a>&gt; ang:<br></div><blockquote class="gmail_quote" style="margin:0px 0px 0px 0.8ex;border-left:1px solid rgb(204,204,204);padding-left:1ex"><u></u> <div> <h3>Re: this is my test 2</h3> <p>qaqa</p> </div> </blockquote></div>';
text = message.value;
newNode.innerHTML = text + "<br> <br>";

live_chat_content.insertBefore(newNode, null);

//console.log(document.getElementsByClassName('gmail_quote'))

//--


let gmail_quote = document.getElementsByClassName('gmail_quote');
let check = "false";
let myindex = 0;

let mybutton = document.createElement('button');

mybutton.setAttribute('id', 'toggle_reply'+live_chat_class);
mybutton.setAttribute('type', 'button');
mybutton.textContent = '...';

for(let i=0;i<gmail_quote.length;i++){
if(gmail_quote[i].parentNode.className == "live_chat"+live_chat_class){

    gmail_quote[i].style.display = 'none';
    check = "true";
    myindex = i;
}

}

gmail_quote[myindex].parentNode.insertBefore(mybutton, gmail_quote[myindex]);

if(check == "false"){
    document.getElementById("toggle_reply"+live_chat_class).style.display = 'none';
}

document.getElementById("toggle_reply"+live_chat_class).addEventListener("click", function(){

for(let i=0;i<gmail_quote.length;i++){
if(gmail_quote[i].parentNode.className == "live_chat"+live_chat_class){

    console.log(gmail_quote[i].parentNode.className);

    if(gmail_quote[i].style.display == 'none'){
        gmail_quote[i].style.display = '';
    }
    else{
        gmail_quote[i].style.display = 'none';
    }

   
}

    
}
});

*/
}


});


message.addEventListener("keypress", function(e){

    
if((e.key == 'Enter' || e.keyCode == 13)){
    
   e.preventDefault();
 //  console.log('not empty');

 if(message.value != ''){
  send.click();
  
 }

   
}


});

//--





@foreach($messages as $message)





var gmail_quote = document.getElementsByClassName('gmail_quote');
var check = "false";
var myindex = 0;
var dots = new Image(30,10);
dots.src = "{{asset('images/threedots.png')}}";

var mybutton = document.createElement('button');
mybutton.setAttribute('id', 'toggle_reply{{$loop->iteration}}');
mybutton.setAttribute('type', 'button');
mybutton.setAttribute('class', 'dots');

mybutton.appendChild(dots);


//mybutton.textContent = '...';


for(let i=0;i<gmail_quote.length;i++){
if(gmail_quote[i].parentNode.className == "live_chat{{$loop->iteration}}"){

    gmail_quote[i].style.display = 'none';
    check = "true";
    myindex = i;
}

}

gmail_quote[myindex].parentNode.insertBefore(mybutton, gmail_quote[myindex]);

if(check == "false"){
    document.getElementById("toggle_reply{{$loop->iteration}}").style.display = 'none';
}

document.getElementById("toggle_reply{{$loop->iteration}}").addEventListener("click", function(){

for(let i=0;i<gmail_quote.length;i++){
if(gmail_quote[i].parentNode.className == "live_chat{{$loop->iteration}}"){

    console.log(gmail_quote[i].parentNode.className);

    if(gmail_quote[i].style.display == 'none'){
        gmail_quote[i].style.display = '';
    }
    else{
        gmail_quote[i].style.display = 'none';
    }

   
}

    
}
});

@endforeach



</script>


<script>

var conn2 = new WebSocket('ws://127.0.0.1:8090/email');


let ex2;
conn2.onopen = function(){

    console.log('connection success2');
    conn2.send('');

};


conn2.onmessage = function(e){

console.log(e.data);

ex2 = e.data;

//timeout2(ex2);

};



setInterval(function(){
    if(ex2 == 'success'){
        ex2 = 'not success';
 
    conn2.send('');

    }
    
  

}, 10);



</script>



</body>
</html>
