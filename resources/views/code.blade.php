<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.header')
</head>
<body>
<br>
<div class="d-flex justify-content-center">

    <form class="px-2 bg-dark text-white rounded" style="width: 30%;" action="{{url('/send_code/'.$email)}}" method="post">
    {{ csrf_field() }}

    <div class="m-3">
<label for="code" class="form-label">Verification Code</label>
        <input class="form-control" type="text" name ="code" id="code" placeholder="Verification Code">

        
        @if($errors->has('code') )
        <div class="form-text text-danger">{{ $errors->first('code') }}</div>
@elseif($errors->has('codeError'))
<div class="form-text text-danger">{{$errors->first('codeError')}}</div>
@endif
<br>
</div>
<div class="m-3">
        <button class="form-control btn btn-primary btn-lg" type="submit">Submit</button>
</div>

<br>
    </form>
    
</div>

<script type="text/javascript" src="{{ asset('js/myapp.js') }}"></script>
</body>
</html>