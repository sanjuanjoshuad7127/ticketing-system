<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ticketMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    public $description;
    public $message_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details, $description, $message_id)
    {
        $this->details = $details;
        $this->description = $description;
        $this->message_id = $message_id;

        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    //  $this->replyTo('s30011003@gmail.com', 'support01');

    $this->subject($this->description);

    if($this->message_id != ''){
        $this->withSwiftMessage(function ($message) {
            $headers = $message->getHeaders();
            $headers->addTextHeader('References', '<'.$this->message_id.'>');
            $headers->addTextHeader('In-Reply-To', '<'.$this->message_id.'>');
        });

    }

      

        return $this->view('mail');
    }
}
