<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class myMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      //  if(!empty($this->attributes['replyTo'])){
        //    $this->replyTo($this->attributes['replyTo'], 'joshua');
       // }
       
      // $this->replyTo('aaronsanjuan1717@gmail.com', 'Verification');

        return $this->subject('Verification Code')->view('mail');
    }
}
