<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Ticket;

use Illuminate\Support\Facades\Mail;

use Ratchet\MessageComponentInterface;

use Ratchet\ConnectionInterface;


class SendController extends Controller implements MessageComponentInterface
{

    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;

    }


    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);


       


    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {

        $data = json_decode($msg);

        $ticket = Ticket::where('message_id', $data->message_id)->first();

        $details = [
            'title' => 'Re: '.$ticket->description,
            'body' => $data->message,
            'replyTo' => env('IMAP_USERNAME'),
            
        ];
    
        $to = [
            [ 

                'name' => $ticket->requester,
                'email' => $data->receiver,
                
            ]
        ];

        $mymail = new \App\Mail\ticketMail($details, 'Re: '.$ticket->description, $data->message_id);

        Mail::to($to)->send($mymail);



    }


    public function onClose(ConnectionInterface $conn)
    {



    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {


    }






}
