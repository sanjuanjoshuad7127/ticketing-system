<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Ratchet\MessageComponentInterface;

use Ratchet\ConnectionInterface;

use Webklex\IMAP\Facades\Client;

use React\EventLoop\LoopInterface;

use App\Models\Ticket;

use App\Models\Live_chat;


class SocketController extends Controller implements MessageComponentInterface
{
    protected $clients;

    public function __construct(LoopInterface $loop)
    {
        $this->clients = new \SplObjectStorage;

    }


    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);


       


    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {

        $data = $msg;



        if($data == ''){






            $oClient = Client::account('default');
            $oClient->connect();        
            $Folders = $oClient->getFolder('INBOX');
    
            $myfolders = $Folders->query()->unseen()->markAsRead()->get();
    //$myfolders = $Folders->query()->from('sanjuanjoshuad7127@gmail.com')->get();
    
                
    
    foreach($myfolders as $myfolder){
    
    
    
        $customer_email = (($myfolder->getHeader()->from[0]->mail == env('IMAP_USERNAME') && count((array)$myfolder->getTo()) > 1)?$myfolder->getHeader()->to[0]->mail:$myfolder->getHeader()->from[0]->mail);
        $requester = (($myfolder->getHeader()->from[0]->mail == env('IMAP_USERNAME') && count((array)$myfolder->getTo()) > 1)?$myfolder->getHeader()->to[0]->personal:$myfolder->getHeader()->from[0]->personal);
    
    
            if(empty($myfolder->getReferences()[0])){
    
                
    
                $data = [];
                $data['customer_email'] = $customer_email;
                $data['description'] = $myfolder->getSubject();
                $data['requester'] = $requester;
                $data['message_id'] = $myfolder->getMessageId();
                $ticket = Ticket::create($data);
                $ticket->save();
                
                $data2 = [];
                $data2['sender'] = $customer_email;
                $data2['description'] = $myfolder->getSubject();
                $data2['message_id'] = $myfolder->getMessageId();
                $data2['receiver'] = env('IMAP_USERNAME');
                $data2['message'] = $myfolder->getHTMLBody();
                $data2['is_sent'] = '1';
                $live_chat = Live_chat::create($data2);
                $live_chat->save();
    
            }
            else{
    
                $data2 = [];
                $data2['sender'] = $customer_email;
                $data2['description'] = $myfolder->getSubject();
                $data2['message_id'] = $myfolder->getReferences()[0];
                $data2['receiver'] = env('IMAP_USERNAME');
                $data2['message'] = $myfolder->getHTMLBody();
                $data2['is_sent'] = '1';
                $live_chat = Live_chat::create($data2);
                $live_chat->save();
    
    
            }




        
        }
        

       





        }

        
        $data = 'success';

        
        foreach($this->clients as $client)
        {
         //   if($client->resourceId != $conn->resourceId){
            
                $client->send($data);
        //    }
            

        }
        




    }

    public function onClose(ConnectionInterface $conn)
    {



    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {


    }



}
