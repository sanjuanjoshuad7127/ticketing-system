<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use React\EventLoop\LoopInterface;

use App\Models\Ticket;

use App\Models\Live_chat;

use Illuminate\Support\Facades\Mail;

use Ratchet\MessageComponentInterface;

use Ratchet\ConnectionInterface;


class MessageController extends Controller implements MessageComponentInterface
{
    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;

    }


    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);


       


    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {

        $data = json_decode($msg);



        if(is_object($data)){
            if($data->sent == 'not sent'){

        $live_chats = Live_chat::where('is_sent', '1')->where('message_id', $data->message_id)->update(['is_sent' => '0']);

      
        foreach($this->clients as $client)
        {
    
        $client->send('zero');


        }

            }
            else{

                //  if($data == 'live_chat'){
        $live_chats = Live_chat::where('is_sent', '1')->where('message_id', $data->message_id)->get();

        $chat_object = new \stdClass();


        foreach($live_chats as $live_chat){
          //  if($live_chat->is_sent == '1'){
                
                $chat_object->name = 'live_chat';
                $chat_object->message = $live_chat->message;
                $chat_object->sender = $live_chat->sender;
                $chat_object->receiver = $live_chat->receiver;
             //   $chat_object->message_id = $live_chat->message_id;

                foreach($this->clients as $client)
                {
            
                $client->send(json_encode($chat_object));
        

                }
                $live_chat_edit = Live_chat::where('id', $live_chat->id)->firstOrFail();
                $live_chat_edit->is_sent = '0';
                $live_chat_edit->update();


        //    }

        }
   // }


            }

            if($data->sent == 'sent'){

                $ticket = Ticket::where('message_id', $data->message_id)->first();


                $data2 = [];
                $data2['sender'] = $data->sender;
                $data2['description'] = 'Re: '.$ticket->description;
                $data2['message_id'] = $data->message_id;
                $data2['receiver'] = $data->receiver;
                $data2['message'] = $data->message;
                $data2['is_sent'] = '1';
                $live_chat = Live_chat::create($data2);
                $live_chat->save();


                /*
                $details = [
                    'title' => 'Re: '.$ticket->description,
                    'body' => $data->message,
                    'replyTo' => env('IMAP_USERNAME'),
                    
                ];
            
                $to = [
                    [ 
        
                        'name' => $ticket->requester,
                        'email' => $data->receiver,
                        
                    ]
                ];
        
                $mymail = new \App\Mail\ticketMail($details, 'Re: '.$ticket->description, $data->message_id);
        
                Mail::to($to)->send($mymail);
                */


            }
            


        }

        $data = 'success';

        
        foreach($this->clients as $client)
        {
         //   if($client->resourceId != $conn->resourceId){
            
                $client->send($data);
        //    }
            

        }




    }


    public function onClose(ConnectionInterface $conn)
    {



    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {


    }







}
