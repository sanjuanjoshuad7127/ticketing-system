<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Ticket;
use App\Models\Live_chat;
use App\Models\Customer;
use Laravel\Socialite\Facades\Socialite;
//use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Mail;
use Webklex\IMAP\Facades\Client;

class TicketController extends Controller
{
    //

    public function index(){

      //  $live_chat = Live_chat::orderBy('id', 'DESC')->first();


        return view('ticket');
    }


    public function post_ticket(Request $request){

        /*
        $data = request()->all();
        $data['column9'] = 'aaaa';
        B2::create($data);
        */

        
        $messageBag = new MessageBag;
        $validator = Validator::make($request->all(), [
            
            'description' => 'required | min:5',
            'message' =>'required',

            
        ]);

        if($validator->fails()){
            $validator->validate();  
        }
        else{

            $details = [
                'title' => $request->input('description'),
                'body' => $request->input('message'),
                'replyTo' => session()->get('customer')->email,
                
            ];
        
            $to = [
                [ 

                    'name' => session()->get('customer')->name,
                    'email' => session()->get('customer')->email,
                    
                ],
                [
                    'name' => env('MAIL_FROM_NAME'),
                    'email' => env('MAIL_USERNAME'),

                ]
            ];

            $mymail = new \App\Mail\ticketMail($details, $request->input('description'), '');

            Mail::to($to)->send($mymail);

            /*
            $message_id = "";


            $data = $request->except('message');
            $data['customer_email'] = session()->get('user_gmail')->email;
            $data['requester'] = session()->get('user_gmail')->name;
            $data['message_id'] = $message_id;

            $ticket = Ticket::create($data);
            $ticket->save();

            */
            
            return redirect('/');

        }




        

    }


    public function tickets(){
        $tickets = Ticket::all();


        return view('tickets')->with(compact('tickets'));

    }


    public function get_live_chat($email, $message_id){
        /*
       $messages = Live_chat::where(function ($query) use($email){
        $query->where(function ($query) use($email) {
            $query->where('sender', '=', env('IMAP_USERNAME'))->Where('receiver', '=', $email);
        })->orWhere(function ($query) use($email) {
            $query->where('sender', '=', $email)->Where('receiver', '=', env('IMAP_USERNAME'));
        });
       })->where('message_id', $message_id)->get();
    */
        $messages = Live_chat::where('message_id', $message_id)->get();
        $ticket = Ticket::where('message_id', $message_id)->first();
    
       


        return view('live_chat')->with(compact('email', 'message_id', 'messages', 'ticket'));


    }



    public function support_ticket(){
    
       // $user = Socialite::driver('google')->user();

        return view('support_ticket');


    }


    public function redirect_to_google(){

        return Socialite::driver('google')->redirect();
    }



    public function get_ticket_email(){
        

        $oClient = Client::account('default');
        $oClient->connect();        
        $Folders = $oClient->getFolder('INBOX');

        $myfolders = $Folders->query()->unseen()->markAsRead()->get();
//$myfolders = $Folders->query()->from('sanjuanjoshuad7127@gmail.com')->get();

            

foreach($myfolders as $myfolder){



    $customer_email = (($myfolder->getHeader()->from[0]->mail == env('IMAP_USERNAME') && count((array)$myfolder->getTo()) > 1)?$myfolder->getHeader()->to[0]->mail:$myfolder->getHeader()->from[0]->mail);
    $requester = (($myfolder->getHeader()->from[0]->mail == env('IMAP_USERNAME') && count((array)$myfolder->getTo()) > 1)?$myfolder->getHeader()->to[0]->personal:$myfolder->getHeader()->from[0]->personal);


        if(empty($myfolder->getReferences()[0])){

            

            $data = [];
            $data['customer_email'] = $customer_email;
            $data['description'] = $myfolder->getSubject();
            $data['requester'] = $requester;
            $data['message_id'] = $myfolder->getMessageId();
            $ticket = Ticket::create($data);
            $ticket->save();
            
            $data2 = [];
            $data2['sender'] = $customer_email;
            $data2['description'] = $myfolder->getSubject();
            $data2['message_id'] = $myfolder->getMessageId();
            $data2['receiver'] = env('IMAP_USERNAME');
            $data2['message'] = $myfolder->getHTMLBody();
            $live_chat = Live_chat::create($data2);
            $live_chat->save();

        }
        else{

            $data2 = [];
            $data2['sender'] = $customer_email;
            $data2['description'] = $myfolder->getSubject();
            $data2['message_id'] = $myfolder->getReferences()[0];
            $data2['receiver'] = env('IMAP_USERNAME');
            $data2['message'] = $myfolder->getHTMLBody();
            $live_chat = Live_chat::create($data2);
            $live_chat->save();


        }
    
    
    

}


return 0;



    }


    public function send_message(Request $request, $email, $message_id){

        $ticket = Ticket::where('message_id', $message_id)->firstOrFail();

        $details = [
            'title' => 'Re: '.$ticket->description,
            'body' => $request->input('message'),
            'replyTo' => env('IMAP_USERNAME'),
            
        ];
    
        $to = [
            [ 

                'name' => $ticket->requester,
                'email' => $email,
                
            ]
        ];

        $mymail = new \App\Mail\ticketMail($details, 'Re: '.$ticket->description, $message_id);

        Mail::to($to)->send($mymail);
        
        $data2 = [];
        $data2['sender'] = env('IMAP_USERNAME');
        $data2['description'] = 'Re: '.$ticket->description;
        $data2['message_id'] = $message_id;
        $data2['receiver'] = $email;
        $data2['message'] = $request->input('message');
        $live_chat = Live_chat::create($data2);
        $live_chat->save();
        


        return redirect()->back();

    }
    
    
    public function customer_tickets(){
        $tickets = Ticket::where('customer_email', session()->get('customer')->email)->get();

        return view('customer_tickets')->with(compact('tickets'));
    }


    public function get_support_register(){

        return view('support_register');
    }

    public function post_support_register(Request $request){

        $customer = Customer::where('email', $request->email);
        $messageBag = new MessageBag;
         $validator = Validator::make($request->all(), [
             'name' => 'required | min:3',
             'email' => 'required | email',
             'pass' => 'required | min:5',
             'cpass' => 'required | min:5',
             
         ], 
         ['name.required' => 'Name Field Empty', ]);
 
        
         if($customer->exists()){
             $messageBag->add('emailError', 'Email Already Exists');
            
         }
         
 
         if($request->pass != $request->cpass && strlen($request->input('pass'))){
             $messageBag->add('passError', 'The password you entered does not match the confirmed password');
         }
 
         if($messageBag->isNotEmpty() || $validator->fails()){
 
            return redirect()->back()->withErrors($validator->errors()->merge($messageBag));
        
 
         }
         else{
             $customer = new Customer();
             $customer->name = $request-> input('name');
             $customer->email = $request-> input('email');
             $customer->pass = $request-> input('pass');
             $customer->save();
     
     
             return redirect('/support_ticket/email/'.$request->input('email'));
         }
 

    }


    public function email($email){


        $array_num = [];

        for($i=0; $i<6; $i++){
            $nums = rand(0,9);
            array_push($array_num, (string)$nums);

        }

       $random_num = implode("",$array_num);

       $customer = Customer::where('email', $email)->firstOrFail();

        $customer->code = $random_num;
        $customer->update();

        $details = [
            'title' => 'Your verification code is ',
            'body' => $random_num,
            'replyTo' => $email
            
        ];
    
        $to = [
            [ 
                'email' => $customer->email,
                'name' => $customer->name,
            ]
        ];

        $mymail = new \App\Mail\myMail($details);
       
       
        Mail::to($to)->send($mymail);

        return redirect('/support_ticket/code/'.$email);


    }

    public function code($email){



        return view('support_code')->with('email', $email);
    }

    public function send_code(Request $request, $email){

        $customer = Customer::where('email', $email)->firstOrFail();

        $messageBag = new MessageBag;
        $validator = Validator::make($request->all(), [
            'code' => 'required | min:6',
            
        
            
        ], 
        ['code.required' => 'Code Field Empty', ]);

        if($validator->fails()){

            $validator->validate(); 
        }
        else if($request->input('code') != $customer->code){

            $messageBag->add('codeError', 'Your verification code is invalid');
            return redirect()->back()->withErrors($messageBag);
        }
        else{
            $request->session()->put('customer', $customer);
            return redirect('/support_ticket');
        }


    }


    public function get_login(){

        return view('support_login');
    }


    public function login(Request $request){
        $customer = Customer::where('email', $request->email);
        $messageBag = new MessageBag;
        $validator = Validator::make($request->all(), [
        
            'email' => 'required | email',
            'pass' => 'required | min:5',
            
        ]);

        if($customer->doesntExist()){
            $messageBag->add('emailError', 'Email Does not Exist');
            
        }
        else{
            if($customer->first()->pass != $request->pass){
                $messageBag->add('passError', 'Incorrect Password');
            }
        }
        

        if($messageBag->isNotEmpty() || $validator->fails()){
            return redirect()->back()->withErrors($validator->errors()->merge($messageBag));
        }
        else{
            return redirect('/support_ticket/email/'.$request-> input('email'));

        }




        


    }



    public function update_live_chat(Request $request, $message_id){

        $validator = Validator::make($request->all(), [
            'reported_by' => 'required | min:5',
            'priority' => 'required',
            'module' => 'required | min:5',
            'date_reported' => 'required',
            'date_accomplishment' => 'required',
            

            
        ]);

        $ticket = Ticket::where('message_id', $message_id)->first();


        if($validator->fails()){

            $validator->validate();
        
 
         }
         else{
            $ticket->reported_by = $request->reported_by;
            $ticket->priority = $request->priority;
            $ticket->module = $request->module;
            $ticket->date_reported = $request->date_reported;
            $ticket->date_accomplishment = $request->date_accomplishment;
            $ticket->type = $request->type;
            $ticket->status = $request->status;
            $ticket->severity = $request->severity;
            $ticket->remarks = $request->remarks;
            $ticket->update();
            return redirect()->back();

         }



    }



   
    public function customers(){
        /*
        Task::select('category_id')
        ->where('user_id',Auth::user()->id)
        ->groupBy(['category_id'])
        ->get()
        ->count();
        */

        $customers = Ticket::select(['requester', 'customer_email', \DB::raw("count(*) as total")])->groupBy(['requester', 'customer_email'])->get();

        return view('customers')->with(compact('customers'));
    }

    public function customers_tickets($email){

        $tickets = Ticket::where('customer_email', $email)->get();

        return view('customer_tickets')->with(compact('tickets'));

    }





}
