<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
//use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Mail;
use App\Models\Ticket;


class UserController extends Controller
{
    //

    public function index(){
        

        if(session()->get('user') == NULL){
            return redirect('/login');
        }

        $tickets = Ticket::all();

        return view('home')->with(compact('tickets'));
    }

    public function get_register(){

        return view('register');
    }

    public function register(Request $request){

       $user = User::where('email', $request->email);
       $messageBag = new MessageBag;
        $validator = Validator::make($request->all(), [
            'name' => 'required | min:3',
            'email' => 'required | email',
            'pass' => 'required | min:5',
            'cpass' => 'required | min:5',
            
        ], 
        ['name.required' => 'Name Field Empty', ]);

       // if ($validator->fails()) {
           
         //   $validator->validate(); 
       // }
        if($user->exists()){
            $messageBag->add('emailError', 'Email Already Exists');
           // return redirect()->back()->withErrors($messageBag);
        }
        else if(preg_match('/(.)+@neotechasia.com/u', $request->input('email')) == 0){
            $messageBag->add('emailError', 'Your domain is incorrect. If you are a customer please visit localhost.com/support_ticket');

        }

        if($request->pass != $request->cpass && strlen($request->input('pass'))){
            $messageBag->add('passError', 'The password you entered does not match the confirmed password');
        }

        if($messageBag->isNotEmpty() || $validator->fails()){

           return redirect()->back()->withErrors($validator->errors()->merge($messageBag));
       //  return redirect()->back()->withErrors($messageBag)->withErrors($validator);

        }
        else{
            $user = new User();
            $user->name = $request-> input('name');
            $user->email = $request-> input('email');
            $user->pass = $request-> input('pass');
            $user->save();
    
    
            return redirect('/email/'.$request-> input('email'));
        }


        /*

        

        */
        

    }

    public function email($email){


        $array_num = [];

        for($i=0; $i<6; $i++){
            $nums = rand(0,9);
            array_push($array_num, (string)$nums);

        }

       $random_num = implode("",$array_num);

       $user = User::where('email', $email)->firstOrFail();

        $user->code = $random_num;
        $user->update();

        $details = [
            'title' => 'Your verification code is ',
            'body' => $random_num,
            'replyTo' => $email
            
        ];
    
        $to = [
            [ 
                'email' => $user->email,
                'name' => $user->name,
            ]
        ];

        $mymail = new \App\Mail\myMail($details);
       // $mymail->replyTo($email);
       
        Mail::to($to)->send($mymail);

        return redirect('/code/'.$email);


    }


    public function code($email){



        return view('code')->with('email', $email);
    }

    public function send_code(Request $request, $email){

        $user = User::where('email', $email)->firstOrFail();

        $messageBag = new MessageBag;
        $validator = Validator::make($request->all(), [
            'code' => 'required | min:6',
            
        
            
        ], 
        ['code.required' => 'Code Field Empty', ]);

        if($validator->fails()){

            $validator->validate(); 
        }
        else if($request->input('code') != $user->code){

            $messageBag->add('codeError', 'Your verification code is invalid');
            return redirect()->back()->withErrors($messageBag);
        }
        else{
            $request->session()->put('user', $user);
            return redirect('/');
        }


    }

    public function get_login(){

        return view('login');
    }


    public function login(Request $request){
        $user = User::where('email', $request->email);
        $messageBag = new MessageBag;
        $validator = Validator::make($request->all(), [
        
            'email' => 'required | email',
            'pass' => 'required | min:5',
            
        ]);

        
        if($user->doesntExist()){
            $messageBag->add('emailError', 'Email Does not Exist');
            
        }
        else{
            if($user->first()->pass != $request->pass){
                $messageBag->add('passError', 'Incorrect Password');
            }
        }
        

        if($messageBag->isNotEmpty() || $validator->fails()){
            return redirect()->back()->withErrors($validator->errors()->merge($messageBag));
        }
        else{
            return redirect('/email/'.$request-> input('email'));

        }

        

    }

    public function receive_email(){

        return view('receive_email');
    }



    




}
