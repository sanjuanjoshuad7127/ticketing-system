<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Ratchet\Server\IoServer;

use Ratchet\Http\HttpServer;

use Ratchet\WebSocket\WsServer;

use Ratchet\Http\OriginCheck;

use Ratchet\Http\Router;

use React\EventLoop\Factory as LoopFactory;

use App\Http\Controllers\SocketController;

use App\Http\Controllers\MessageController;

use Symfony\Component\Routing\Matcher\UrlMatcher;

use Symfony\Component\Routing\RequestContext;

use Symfony\Component\Routing\Route;

use Symfony\Component\Routing\RouteCollection;

use React\Socket\Server;


class WebSocketServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'websocket:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /*
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new SocketController()
                )
            ),
            8090
        );

        $server->run();
        */

        $port = 8090;
        $address = '0.0.0.0'; // Client address to accept. (0.0.0.0 means receive connections from any)
        $loop = LoopFactory::create();
        $socket = new Server("{$address}:{$port}", $loop);
        $routes = new RouteCollection();


       // echo "App route-one websocket running on localhost:{$port}/route-one \n";
        $SocketController = new WsServer(new SocketController($loop));
        $SocketController->enableKeepAlive($loop); // Enable message ping:pong
        $routes->add('email', new Route('/email', [
            '_controller' => $SocketController,
        ]));

/*
        $MessageController = new WsServer(new MessageController($loop));
        $MessageController->enableKeepAlive($loop); // Enable message ping:pong
        $routes->add('message', new Route('/message', [
            '_controller' => $MessageController,
        ]));
        */


        $urlMatcher = new UrlMatcher($routes, new RequestContext());
        $router = new Router($urlMatcher);
        $checkedApp = new OriginCheck($router, ['localhost']);
        $server = new IoServer(new HttpServer($router), $socket, $loop); // Pass $checkedApp to filter origin
        $server->run();





    }
}
