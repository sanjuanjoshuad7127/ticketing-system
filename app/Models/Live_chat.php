<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Live_chat extends Model
{
    use HasFactory;

    protected $table = 'live_chat';
    protected $primaryKey = 'id';
    protected $fillable = ['message_id', 'sender', 'receiver', 'description', 'message', 'is_sent'];
    
    public $timestamps = false;



}
