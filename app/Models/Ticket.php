<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $table = 'ticket';
    protected $primaryKey = 'id';
    protected $fillable = ['requester', 'message_id', 'reported_by', 'description', 'type', 'status', 'severity', 'priority', 'module', 'remarks', 'date_reported', 'date_accomplishment', 'customer_email'];
    
    public $timestamps = false;



}
